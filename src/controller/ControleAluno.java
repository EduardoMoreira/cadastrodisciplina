package controller;

import java.util.ArrayList;

import model.Aluno;
public class ControleAluno{
	
	//Atributos
	private ArrayList<Aluno> listaAlunos;
	
	//Construtor
	public ControleAluno (){
		listaAlunos = new ArrayList<Aluno>();
	}
	
	//Métodos
	public String adicionar(Aluno umAluno){
		listaAlunos.add(umAluno);
		String mensagem = "Aluno cadastrado com sucesso!";
		return mensagem;
	}

	public String remover(Aluno umAluno){
		listaAlunos.remove(umAluno);
		String mensagem = "Aluno removido com sucesso!";
		return mensagem;
	}
	
	public Aluno pesquisar(String umNome){
		for(Aluno umAluno:listaAlunos){
      			if(umAluno.getNome().equalsIgnoreCase(umNome)){
        		return umAluno;
      			}
    		}
    		return null;
  	}

  	public int pesquisarGeral(){
    		int x=0;
		for(Aluno umAluno: listaAlunos){
      			x = x + 1;
    		}
    		return x;
	}
  
  	public void pesquisarExibir(){
    		for(Aluno umAluno:listaAlunos){
      			System.out.println ("Aluno: " + umAluno.getNome() + " - Matricula: " + umAluno.getMatricula());
    		}
  	}
}
